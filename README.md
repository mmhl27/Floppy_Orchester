## Floppy Orchester

Bei dem hiesigen Projekt handelt es sich um eine Gruppenarbeit im Fach Physical Computing.

Das Floppy Orchester (im folgenden FO genannt) ist ein Arduino YUN Mikrokontroller, welcher 
mittels eines kleinen Webservers die Möglichkeit bietet, MIDI Dateien hochzuladen welche dann auf 8 Floppy Laufwerken abgespielt werden.
