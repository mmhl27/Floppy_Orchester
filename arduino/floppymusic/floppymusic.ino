#include <TimerOne.h>
#include <FloppyMusician.h>

FloppyMusician floppys[8] = {FloppyMusician(2, 3), FloppyMusician(4, 5), FloppyMusician(6, 7), FloppyMusician(8, 9),
                             FloppyMusician(10, 11), FloppyMusician(12, 13), FloppyMusician(A0, A1), FloppyMusician(A2, A3)
                            };
uint8_t r_channel;
uint16_t r_period;
boolean firstRun = true;

void setup() {
  pinMode(2, OUTPUT); // Step control 1
  pinMode(3, OUTPUT); // Direction 1
  pinMode(4, OUTPUT); // Step control 2
  pinMode(5, OUTPUT); // Direction 2
  pinMode(6, OUTPUT); // Step control 3
  pinMode(7, OUTPUT); // Direction 3
  pinMode(8, OUTPUT); // Step control 4
  pinMode(9, OUTPUT); // Direction 4
  pinMode(10, OUTPUT); // Step control 5
  pinMode(11, OUTPUT); // Direction 5
  pinMode(12, OUTPUT); // Step control 6
  pinMode(13, OUTPUT); // Direction 6
  pinMode(A0, OUTPUT); // Step control 7
  pinMode(A1, OUTPUT); // Direction 7
  pinMode(A2, OUTPUT); // Step control 8
  pinMode(A3, OUTPUT); // Direction 8
  delay(60000);
  Timer1.initialize(40);
  Timer1.attachInterrupt(tick); 
  Serial1.begin(9600);
}

void loop() 
{
  if (firstRun)
  {
    firstRun = false;
    resetAllDevices();
    delay(2000);    
  }
  if (Serial1.available() > 2) 
  {
    //Watch for special 100-message to reset the drives
    if (Serial1.peek() == 100)
    {
      resetAllDevices();
      //Flush any remaining messages.
      while (Serial1.available() > 0) 
      {
        Serial1.read();
      }
    }
    else 
    {
      r_channel = Serial1.read();
      r_period = Serial1.read() << 8;
      r_period |= Serial1.read();
      if(r_channel<8)
      {
        floppys[r_channel].setPeriod(r_period);
      }
      
      //currentPeriod[Serial.read()] = (Serial.read() << 8) | Serial.read();
    }
  }
}

void tick()
{
  for (int i = 0; i < 8; i++)
  {
    floppys[i].tick();
  }
}

void testReset()
{
  for(int k=0; k<640; k++)
  {
    floppys[k%8].stepback();
    delay(2);
  }
}


void resetAllDevices()
{
  for(byte i = 0; i < 8; i++)
  {
    floppys[i].halt();
  }
  for(int k=0; k<640; k++)
  {
    floppys[k%8].stepback();
    delay(2);
  }
  for(int i = 0; i < 8; i++)
  {
    floppys[i].resetstatus();
  }
}

