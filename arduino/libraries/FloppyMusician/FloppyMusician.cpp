#include "Arduino.h"
#include <inttypes.h>
#include "FloppyMusician.h"

FloppyMusician::FloppyMusician(uint8_t spin, uint8_t dpin)
{
	this->directionpin=dpin;
	this->steppin=spin;
}

void FloppyMusician::toggleDirection(void)
{
	if(this->direction==LOW)
	{
		this->direction=HIGH;
		digitalWrite(this->directionpin,HIGH);
	}
	else
	{
		this->direction=LOW;
		digitalWrite(this->directionpin,LOW);
	}
	return;
}

void FloppyMusician::step(void)
{	
	this->currentPosition++;
	if(this->currentPosition >= MAXPOS)
	{
		this->toggleDirection();
		this->currentPosition=0;
	}
	if(this->stepstate==LOW)
	{
		this->stepstate=HIGH;
		digitalWrite(this->steppin,HIGH);
	}
	else
	{
		this->stepstate=LOW;
		digitalWrite(this->steppin,LOW);
	}
	return;
}

void FloppyMusician::stepback(void)
{	
	digitalWrite(this->directionpin,HIGH);
    digitalWrite(this->steppin,HIGH);
    digitalWrite(this->steppin,LOW);
}

void FloppyMusician::resetstatus(void)
{
	this->direction = LOW;
	this->currentPeriod = 0;
	this->currentPosition = 0;
	this->currentTick = 0;
	return;
}

void FloppyMusician::halt(void)
{
	this->currentPeriod = 0;
	return;
}

void FloppyMusician::tick(void)
{
	if(this->currentPeriod > 0)
	{
		this->currentTick++;
		if(this->currentTick >= this->currentPeriod)
		{
			this->step();
			this->currentTick = 0;
		}
	}
	return;
}

void FloppyMusician::setPeriod(uint16_t period)
{
	this->currentPeriod = period;
	return;
}
