#ifndef FloppyMusician_h
#define FloppyMusician_h


#include "Arduino.h"
#include <inttypes.h>

#define FWD_DIR 1
#define BCK_DIR 0
#define MAXPOS 158

/* FloppyMusician repräsentiert ein Flopppylaufwerk, mit dem
 * Zweck Musik durch den Schrittmotor zu erzeugen
 * Eine Steuerung von Außen erfolgt weitestgehend über setPeriod()
 * und tick().
 * currentPeriod gibt an, wie viele ticks, der vom Nutzer gewählten Dauer
 * nötig sind um eine Note zu spielen.
 * Der Anwender sorgt dafür, dass tick() in regelmäßigen, gleichbleibenden
 * Abständen aufgerufen wird, der Steppermotor bewegt sich um einen Schritt,
 * sobald currentTick >= currentPeriod ist.
 * Um sinnvolle Töne zu erzeugen, sollten dem Anwender die nötigen
 * Timings bekannt sein. 
 */
class FloppyMusician
{
	public:
		/* Parameter spin = Pinnummer des Schrittpins,
		 * 			 dpin = Pinnummer des Richtungspins des Schrittmotors
		 * 			 beide Pins müssen über digitalWrite() schreibbar sein.
		 */
		FloppyMusician(uint8_t spin, uint8_t dpin);
		
		/*
		 * Diese Methode ist zum (initialen) resetten gedacht,
		 * sie fährt den Motor des Laufwerks genaueinen Schritt zurück,
		 * interne Statusänderungen werden nicht vorgenommen!
		 */
		void stepback(void);
		
		/* tick() muss in regelmäßigen gleichbleibenden Zeitabständen
		 * durch den Anwender aufgerufen werden, empfohlen sind etwa 40µs.
		 * tick zählt Schritte bis zum Erreichen des Threshholds der jeweiligen
		 * Note und fährt daraufhin einen Schritt (ruft step() auf)
		 * currentPeriod muss > 0 sein.
		 */
		void tick(void);
		
		/* halt() setzt currentPeriod auf 0 und stoppt somit
		 * das Abspielen auf diesem Laufwerk.
		 */
		void halt(void);
		
		/* Die Methode resetstatus() dient zur initialisierung der internen
		 * Werte zur darstellung des Floppylaufwerks.
		 * Sie muss einnmal zwingend nach dem initialisieren durch
		 * stepback() und vor dem gebrauch von tick() oder step() 
		 * aufgerufen werden.
		 */
		void resetstatus(void);
		
		/* step() kann benutzt werden, um das Laufwerk einen Schritt
		 * zu bewegen, die Richtung wird dabei bei erreichen der maximalen
		 * Auslenkung automatisch geändert. Es ist daher nötig vor der
		 * ersten Verwendung resetstatus() aufzurufen.
		 */
		void step(void);
		
		/* setzt die zur Tickrate korrespondierende ziel Zahl an Ticks
		 * um die vom Anwender gewünschte Note zu spielen.
		 */
		void setPeriod(uint16_t period);
				
	private:
		void toggleDirection(void);
	
		uint8_t currentPeriod;
		uint8_t currentTick;
		uint16_t currentPosition;
	
		uint8_t directionpin;
		uint8_t steppin;
		
		int direction;
		int stepstate;
};

#endif

