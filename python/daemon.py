#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import time
import atexit
from signal import SIGTERM


class FloppyDaemon:
    def __init__(self):
        self.stdin = '/dev/null'
        self.stdout = '/var/log/Floppyorchester.log'
        self.stderr = '/dev/null'
        self.pidfile = '/var/run/Floppyorchester/pid'

        self.playing = False
        self.playthread = None

        # self.inputpipe='/var/run/Floppyorchester/in'
        # self.outputpipe='/var/run/Floppyorchester/out'

        self.inputpipe = './in'
        self.outputpipe = './out'

    def daemonize(self):

        if os.geteuid() != 0:
            print("Daemon benötigt root privilegien. Abbruch")
            sys.exit(1)
        try:
            pid = os.fork()
            if pid != 0:
                sys.exit(0)
        except OSError:
            print("Daemon konnte nicht gestartet werden Fork#1 fehlgeschlagen")
            sys.exit(1)

        os.chdir("/")
        os.setsid()
        os.umask(0)

        try:
            pid = os.fork()
            if pid != 0:
                sys.exit(0)
        except OSError:
            print("Zweiter Fork Fehlgeschlagen.")
            sys.exit(1)

        sys.stdout.flush()
        sys.stderr.flush()

        os.dup2(file(self.stdin, 'r').fileno(), sys.stdin.fileno())
        os.dup2(file(self.stdout, 'a+').fileno(), sys.stdout.fileno())
        os.dup2(file(self.stderr, 'a+', 0).fileno(), sys.stderr.fileno())

        atexit.register(self.cleanup)
        file(self.pidfile, 'w+').write("%s\n" % str(os.getpid()))
        try:
            os.mkfifo(self.inputpipe)
            os.mkfifo(self.outputpipe)
        except OSError:
            print("Pipes konnten nicht angelegt werden.")
            sys.exit(1)

    def cleanup(self):
        os.remove(self.pidfile)

        # kill playthread

    def start(self):
        print("halloWelt!")
        #



        self.daemonize()

    def shutdown(self):
        try:
            pf = file(self.pidfile, 'r')
            pid = int(pf.read().strip())
            pf.close()
        except IOError:
            pid = -1

        if pid == -1:
            print("Daemon ist nicht gestartet.")
            return

        os.kill(pid, SIGTERM)

    def restart(self):
        self.shutdown()
        self.start()

    def fplay(self, midifile):
        if __name__ == "__main__":
            # open pipes
            ipipe = os.fdopen(os.open(self.outputpipe, os.O_RDONLY | os.O_NONBLOCK))
            opipe = os.fdopen(os.open(self.inputpipe, os.O_WRONLY | os.O_NONBLOCK))
            opipe.write("fplay\n")
            opipe.write(midifile + "\n")
            time.sleep(0.1)
            buf = ipipe.readline()
            if buf == "PLAY_OK\n":
                print("Abspielen gestartet.")
            elif buf == "FILE_NOT_FOUND\n":
                print("Datei wurde nicht gefunden")
            elif buf == "INVALID_FILE\n":
                print("Ungültige Datei")
            elif buf == "NOT_ENOUGH_PARAMETERS\n":
                print("Nicht genügend Parameter")
                # direkt abfangen
            else:
                print(buf)
        else:
            self.playing = True
            # spawn play process

    def fstop(self):
        if __name__ == "__main__":
            ipipe = os.fdopen(os.open(self.outputpipe, os.O_RDONLY | os.O_NONBLOCK))
            opipe = os.fdopen(os.open(self.inputpipe, os.O_WRONLY | os.O_NONBLOCK))
            opipe.write("fstop\n")
            time.sleep(0.1)
            buf = ipipe.readline()
            if buf == "STOP_OK\n":
                print("Abspielen gestoppt.")
            elif buf == "NOT_PLAYING":
                print("Es läuft kein Track.")
            else:
                print(buf)
        else:
            self.playing = False
            # stop play process
            # reset floppydrives

    def fpause(self):
        if __name__ == "__main__":
            ipipe = os.fdopen(os.open(self.outputpipe, os.O_RDONLY | os.O_NONBLOCK))
            opipe = os.fdopen(os.open(self.inputpipe, os.O_WRONLY | os.O_NONBLOCK))
            opipe.write("fpause\n")
            time.sleep(0.1)
            buf = ipipe.readline()
            if buf == "PAUSE_OK":
                print("Track wurde pausiert")
            elif buf == "NOT_PLAYING":
                print("Es wird derzeit kein Track abgespielt")
            else:
                print(buf)

        else:
            self.playing = False

    def fcontinue(self):
        if __name__ == "__main__":
            ipipe = os.fdopen(os.open(self.outputpipe, os.O_RDONLY | os.O_NONBLOCK))
            opipe = os.fdopen(os.open(self.inputpipe, os.O_WRONLY | os.O_NONBLOCK))
            opipe.write("fcontinue\n")
            time.sleep(0.1)
            buf = ipipe.readline()
            if buf == "CONTINUE_OK":
                print("Abspielen von "+ipipe.readline()+" wird fortgesetzt")
            elif buf == "NOT_PAUSED":
                print("Es ist kein Track Pausiert.")
            else:
                print(buf)
        else:
            self.playing = True

    def fnote(self, note, millis=100):
        if __name__ == "__main__":
            ipipe = os.fdopen(os.open(self.outputpipe, os.O_RDONLY | os.O_NONBLOCK))
            opipe = os.fdopen(os.open(self.inputpipe, os.O_WRONLY | os.O_NONBLOCK))
            opipe.write("n" + str(note) + ";" + str(millis) + "\n")
            time.sleep(0.01)
            buf = ipipe.readline()
            if buf == "PLAY_OK":
                print("Note wird gespielt")
            elif buf == "INVALID_FORMAT":
                print("Aufruf hat falsches format")
            elif buf == "INVALID_NOTE":
                print("Note liegt außerhalb des spielbaren Bereichs")
            else:
                print(buf)
        else:
            print("pnote")

    def idle(self):
        ipipe = os.fdopen(os.open(self.inputpipe, os.O_RDONLY | os.O_NONBLOCK))
        opipe = os.fdopen(os.open(self.outputpipe, os.O_WRONLY | os.O_NONBLOCK))
        while True:
            buf = None
            buf = ipipe.readline()
            if buf[0] == "n":
                tmp = buf.strip("n").split(";", 1)


            else:
                if buf == "fplay":
                    self.paused = False
                    time.sleep(0.1)
                    buf = ipipe.readline()
                    # check buf
                    # start playing

                elif buf == "fstop":
                    self.paused = False
                    # stop playing
                    time.sleep(0.1)

                elif buf == "fpause":
                    if self.playing == True:
                        self.paused = True
                        # pause playing
                    time.sleep(0.1)

                elif buf == "fcontinue":
                    if self.paused == True:
                        self.paused = False
                        # continue Playing
                    time.sleep(0.1)

            time.sleep(0.1)


if __name__ == "__main__":
    daemon = FloppyDaemon()

    arglen = len(sys.argv)
    if arglen<2:
        print("too few arguments...todo add help")
        sys.exit(1)

    if sys.argv[1] == 'start':
        daemon.start()
    elif sys.argv[1] == 'shutdown':
        daemon.shutdown()
    elif sys.argv[1] == 'restart':
        daemon.restart()
    else:

        if sys.argv[1] == "fplay":
            print("fplay")
            daemon.fplay("test.midi")
        elif sys.argv[1] == "fstop":
            print("fstop")
            daemon.fstop()
        elif sys.argv[1] == "fpause":
            print("fpause")
            daemon.fpause()
        elif sys.argv[1] == "fcontinue":
            print("fcontinue")
            daemon.fcontinue()
        elif sys.argv[1] == "fnote":
            print("fnote")
            daemon.fnote(10)
        else:
            print("Error unknown command")
