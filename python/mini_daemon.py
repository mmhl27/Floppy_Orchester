#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import time
import atexit
import Queue
import serial
from threading import Thread
from signal import SIGTERM
from datetime import datetime as dt
from mido import MidiFile


# Der FloppyDaemon bildet eine Vermittlungsschicht zwischen
# dem Webserver auf der einen und dem Arduino auf der anderen Seite
# er liest die Midis und timed die Steuerung der Diskettenlaufwerke.
# Der Daemon besteht aus bis zu 4 Threads: Input, Output, Control und Play.
# Die ersten drei Threads sind immer aktiv, der Playthread wird bei bedarf
# gestartet und terminiert selbstständig oder auf Anforderung.
# Zur Steuerung implementiert der Daemon ein Protokoll, siehe Ausarbeitung, bisher
# aber noch nicht vollständig. Desweiteren ist ansatzweise eine Utility Funktion implementiert,
# die eine Steuerung des Daemons über diese Datei erlaubt.
class FloppyDaemon:
    def __init__(self):
        # int period = microPeriods[(message.getMessage()[1] & 0xff)] / (ARDUINO_RESOLUTION * 2);
        self.microsecs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                        30578, 28861, 27242, 25713, 24270, 22909, 21622, 20409, 19263, 18182, 17161, 16198,
                        15289, 14436, 13621, 12856, 12135, 11454, 10811, 10205, 9632, 9091, 8581, 8099,
                        7645, 7218, 6811, 6428, 6068, 5727, 5406, 5103, 4816, 4546, 4291, 4050,
                        3823, 3609, 3406, 3214, 3034, 2864, 2703, 2552, 2408, 2273, 2146, 2025,
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        # Die Folgenden Werde im Array microticks entsprechen der Anzahl, der vom Arduino zu fahrenden Ticks               
        # bei einer Tickdauer von 40 microsekunden. Die Position im Array entspricht dem Wert der jeweiligen
        # Note im MIDI Format. Leere Töne(0) können vom Diskettenlaufwerk nicht richtig erzeugt werden und
        # werden daher beim Abspielen weggelassen. 
        # Daher eigen sich nicht alle MIDI Dateien zum Abspielen auf den Laufwerken.
        self.microticks = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                        382, 361, 341, 321, 303, 286, 270, 255, 240, 227, 215, 202,
                        191, 180, 170, 161, 152, 143, 135, 128, 120, 114, 107, 101,
                        96, 90, 85, 80, 76, 72, 68, 64, 60, 57, 54, 51,
                        48, 45, 43, 40, 38, 36, 34, 32, 30, 28, 27, 25,
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        self.logfile = open("/var/log/floppylog", "w+")
        self.pidfile = '/var/run/Floppyorchester/pid'

        self.playing = False
        self.midifile = 'tetris.mid'

        self._killplaythreadflag = False
        self._firstrun = True
        self._shutdown = False
        self._logginglevel = 0  # 0=normal 1=extra

        self.inputpipe = '/var/run/Floppyorchester/in'
        self.outputpipe = '/var/run/Floppyorchester/out'
        self.iqueue = Queue.Queue()
        self.oqueue = Queue.Queue()
        self.logentry("Daemon init complete")

        self.logentry("[SERIAL]... initializing Serial /dev/ttyATH0")
        self.ser = serial.Serial('/dev/ttyATH0')


        # self.bc = BridgeClient()

    # outthread entnimmt, sobald er gestartet ist der zugehörigen Queue (oqueue)
    # ständig Werte, sofern vorhanden und gibt sie über die Pipe in 
    # /var/run/Floppyorchester/out aus.
    def outthread(self):
        self.logentry("[O-Thread]... started")
        opipe = open("/var/run/Floppyorchester/out", "w")
        self.logentry("[O-Thread]... opened pipe")
        if self._logginglevel == 1:
            self.oqueue.put("Hallo_Oqueue")
        while not self._shutdown:
            if not self.oqueue.empty():
                s = self.oqueue.get() + "\n"
                if self._logginglevel == 1:
                    self.logentry("[O-Thread]... found item: " + s + " on Output-Queue, writing to pipe")
                opipe.write(s)
                opipe.flush()
                if self._logginglevel == 1:
                    self.logentry("[O-Thread]... writing to Pipe finished")
            else:
                time.sleep(0.05)
        opipe.close()
        self.logentry("[O-Thread]... closing")
    
    # inthread entnimmt, sobald er gestartet ist der zugehörigen Pipe
    # /var/run/Floppyorchester/in Werte, sofern vorhanden und legt sie 
    # zur internen Verarbeitung in die Queue (iqueue)
    def inthread(self):
        self.logentry("[I-Thread]... started")                                           
        ipipe = open(self.inputpipe, "r")
        while not self._shutdown:
            s = ipipe.readline()
            s = s[0:len(s)-1]
            self.logentry("[I-Thread]... read line: " + s + "")
            self.iqueue.put(s)
            self.logentry("[I-Thread]... put item on Queue")
        ipipe.close()
        self.logentry("[I-Thread]... closing")

    # Der Playthread wird bei Bedarf gestartet, die MIDI Datei muss dabei vorher nach
    # Protokollspezifikation festgelegt werden.
    # Der Thread lässt sich über self._killplaythreadflag = True beenden, andernfalls
    # Terminiert der Thread nach erfolgtem Abspielen.
    def playthread(self):
        # self.bc.put('s','2') # init status TODO delete
        self.logentry("[P-Thread]... spawned new playthread")
        if self._firstrun:
	    self.ser.write(bytearray([100,0,0]))
            self._firstrun = False
            self.logentry("[P-Thread]... first run, resetting devices.")
            # TODO Laufwerke zurücksetzen

        self.logentry("[P-Thread]... opening midi")
        md = MidiFile(self.midifile)
        snd = bytearray([0x00, 0x00, 0x00])
        for message in md.play():
            if self._killplaythreadflag:
	 	self.ser.write(bytearray([100,0,0]))
                self.playing = False
                self._killplaythreadflag = False
                return 0
            snd[0] = message.channel & 0xFF
            if message.type == "note_off":
                snd[1] = 0
                snd[2] = 0
            elif message.type == "note_on":
                tick = self.microticks[message.note]
                snd[1] = tick >> 8 & 0xFF
                snd[2] = tick & 0xFF
                if message.velocity == 0:
                    snd[0] = 0
                    snd[1] = 0
            self.ser.write(snd)
            # self.oqueue.put(snd[0] + " " + snd[1] + " " + snd[2])
        self.logentry("[P-Thread]... closing")
        self.playing = False
        self._killplaythreadflag = False
        return 0

    # Führt die Daemonisierung des Prozesses durch, notwendige initialisierungen.
    def daemonize(self):
        if os.geteuid() != 0:
            print("Daemon benötigt root privilegien. Abbruch")
            sys.exit(1)
        self.logentry("daemonizing... root privileges aquired")
        if not os.path.exists("/var/run/Floppyorchester"):
            os.makedirs("/var/run/Floppyorchester")
            self.logentry("daemonizing... creating missing directories")
        else:
            self.logentry("daemonizing... found needed directories")
        try:
            self.logentry("daemonizing... attempting fork")
            pid = os.fork()
            if pid != 0:
                self.logentry("daemonizing... exiting parent")
                sys.exit(0)
        except OSError:
            self.logentry("daemonizing... fork failed ABORTING")
            sys.exit(1)

        self.logentry("daemonizing... changing to root directory")
        os.chdir("/")
        self.logentry("daemonizing... setsid")
        os.setsid()
        self.logentry("daemonizing... umasking")
        os.umask(0)

        try:
            self.logentry("daemonizing... attempting second fork")
            pid = os.fork()
            if pid > 0:
                self.logentry("daemonizing... exiting second parent")
                sys.exit(0)
        except OSError:
            self.logentry("daemonizing... second fork failed ABORTING")
            sys.exit(1)
        self.logentry("daemonizing... fork finished")
        self.logentry("daemonizing... closing filedescriptors")
        null = os.open(os.devnull, os.O_RDWR)
        os.dup2(null, sys.stdin.fileno())
        os.dup2(null, sys.stdout.fileno())
        os.dup2(null, sys.stderr.fileno())
        os.close(null)
        self.logentry("daemonizing... registering exit event")
        atexit.register(self.cleanup)
        self.logentry("daemonizing... writing pidfile")
        pf = open(self.pidfile, 'w+')
        pf.write("%s\n" % str(os.getpid()))
        pf.flush()
        pf.close()
        try:
            self.logentry("daemonizing... creating pipes")
            os.mkfifo(self.inputpipe)
            os.mkfifo(self.outputpipe)
            self.logentry("daemonizing... initializing io-Threads")
            tout = Thread(target=self.outthread)
            tin = Thread(target=self.inthread)
            self.logentry("daemonizing... starting io-Threads")
            tout.start()
            tin.start()
        except OSError:
            self.logentry("daemonizing... pipe creation failed ABORTING")
            sys.exit(1)

    # Wird beim beenden des Programms automatisch ausgeführt und bereinigt
    # den Daemon und Umgebung vor dem Verlassen
    def cleanup(self):
        self.logentry("Shutdown ordered... ending Threads")
        self._shutdown = True
        self._killplaythreadflag = True
        time.sleep(0.3)
        self.logentry("Threads ended, deleting temporary files")
        os.remove("/var/run/Floppyorchester/pid")
        os.remove("/var/run/Floppyorchester/in")
        os.remove("/var/run/Floppyorchester/out")

    # Diese Funktion dient zum Starten des Daemons,
    # nach erfolgter Daemonisierung wird der Controlthread ausgeführt.
    def start(self):
        print("hallo Daemon!")
        self.logentry("beginning daemonization")
        self.daemonize()
        self.logentry("daemonization complete")
        self.logentry("startup completed")
        self.control()
    
    # Hilfsfunktion zum Schreiben der logeinträge
    def logentry(self, s):
        self.logfile.write("[" + str(dt.now()) + "]  " + s + "\n")
        self.logfile.flush()                                                 

    # Aufrufen zum Sauberen Beenden des Daemons
    def shutdown(self):
        try:
            pf = file(self.pidfile, 'r')
            pid = int(pf.read().strip())
            pf.close()
        except IOError:
            pid = -1

        if pid == -1:
            print("Daemon ist nicht gestartet.")
            return

        os.kill(pid, SIGTERM)
        os.remove("/var/run/Floppyorchester/pid")
        os.remove("/var/run/Floppyorchester/in")
        os.remove("/var/run/Floppyorchester/out")

    # startet das Daemonprogramm neu
    def restart(self):
        self.shutdown()
        self.start()

    # utility Funktion, falls nicht daemonisiert, dient zur einfachen Steuerung des Daemons,
    # leitet Abspielen der übergebenen MIDI-Datei ein.
    def fplay(self, midifile):
        if __name__ == "__main__":
            # open pipes
            ipipe = open(self.outputpipe, "r")
            opipe = open(self.inputpipe, "w")
            opipe.write("play\n")
            opipe.flush()
            opipe.write(midifile + "\n")
            opipe.flush()
            time.sleep(0.1)
            buf = ipipe.readline()
            if buf == "PLAY_OK\n":
                print("Abspielen gestartet.")
            elif buf == "FILE_NOT_FOUND\n":
                print("Datei wurde nicht gefunden")
            elif buf == "INVALID_FILE\n":
                print("Ungültige Datei")
            elif buf == "NOT_ENOUGH_PARAMETERS\n":
                print("Nicht genügend Parameter")
                # direkt abfangen
            else:
                print(buf)
            ipipe.close()
            opipe.close()

    # utility Funktion, falls nicht daemonisiert, dient zur einfachen Steuerung des Daemons,
    # stoppt eine laufende Wiedergabe.
    def fstop(self):
        if __name__ == "__main__":
            ipipe = open(self.outputpipe, "r")
            opipe = open(self.inputpipe, "w")
            opipe.write("fstop\n")
            opipe.flush()
            time.sleep(0.1)
            buf = ipipe.readline()
            if buf == "STOP_OK\n":
                print("Abspielen gestoppt.")
            elif buf == "NOT_PLAYING":
                print("Es läuft kein Track.")
            else:
                print(buf)
            ipipe.close()
            opipe.close()

    # Hauptthread der Anwendung, implementiert die Logik des Daemons
    # und implementiert das Protokoll zur Kommunikation mit dem Webserver
    # oder anderen Anwendungen.
    def control(self):
        self.logentry("[CONTROL]... starting control-loop")
        while True:
            buf = self.iqueue.get()
            self.logentry("[CONTROL]... queue has item: " + buf)
            if buf == "play":
                self.logentry("[CONTROL]... command <play> called")
                if self.playing:
                    self.logentry("[CONTROL]... allready playing, stopping")
                    self._killplaythreadflag = True
                    time.sleep(0.2)
                    self.logentry("[FSTOP] killed playthread")
                self.playing = True
                self.midifile = self.iqueue.get()
                # TODO fehlerhafte midi abfangen
                pt = Thread(target=self.playthread)
                pt.start()
                self.oqueue.put("PLAY_OK")
            elif buf == "fstop":
                self.logentry("[CONTROL]... command <fstop> called")
                if self.playing:
                    self.playing = False
                    self._killplaythreadflag = True
                    time.sleep(0.2)
                    self.logentry("[FSTOP] killed playthread")
                    self.oqueue.put("STOP_OK")
                else:
                    self.logentry("[CONTROL]... not playing, nothing to do")

            elif buf == "status":
                if self.playing:
                    self.oqueue.put("STATUS_PLAYING")
                else:
                    self.oqueue.put("STATUS_STOPPED")
            else:
                self.logentry("[CONTROL]... unknown command, writing to output pipe")
                self.oqueue.put("ERROR")
            time.sleep(0.1)

if __name__ == "__main__":
    daemon = FloppyDaemon()

    arglen = len(sys.argv)
    if arglen < 2:
        print("too few arguments...todo add help")
        sys.exit(1)

    if sys.argv[1] == 'start':
        daemon.start()
    elif sys.argv[1] == 'shutdown':
        daemon.shutdown()
    elif sys.argv[1] == 'restart':
        daemon.restart()
    elif sys.argv[1] == "fplay":
        print("fplay")
        daemon.fplay("test.midi")
    elif sys.argv[1] == "fstop":
        print("fstop")
        daemon.fstop()
    elif sys.argv[1] == "status":
        print("not implemented")
        # TODO implement status
    else:
        print("Error unknown command or too few arguments")
