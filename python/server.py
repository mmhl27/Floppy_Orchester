#!/usr/bin/env python2
# -*- coding: utf-8 -*-


## Import der benötigten Module
# bottle ist ein Microframework um Webanwendungen in Python zu erstellen
from bottle import Bottle, run, request, template, static_file, redirect
import os, sys

## Hier wird der Pfad ermittelt unter welchem der Server läuft. Dadurch können weiterhin relative Pfadangaben genutzt werden
SERVERPATH = os.path.dirname(os.path.realpath(__file__))

PIPE_TO = '/var/run/Floppyorchester/in'
PIPE_FROM = '/var/run/Floppyorchester/out'

## Erstellen einer Instanz von Bottle mit dem Namen app
app = Bottle()

## Hier beginnt das Anlegen der Routen, in diesem Fall der Route die statischen Inhalt ausliefert (Bilder, CSS, etc.)
# Die Route nutzt die static_file Methode von Bottle um dem Server mitzuteilen, dass es sich bei dem angegebenen Dateipfad um statischen Inhalt handelt
# Hierbei kann ein root-Verzeichnis deklariert werden, wodurch bottle automatisch auch Unterverzeichnisse mit einbezieht.
@app.route('/static/<filepath:path>')
def server_static(filepath):
	return static_file(filepath, root=SERVERPATH + '/static/')


## Standardroute welche als erstes Vom Server bereitgestellt wird
# Sämtliche Routen haben als return Wert den auszuliefernden Inhalt (Dateien, HTML oder Templates)
@app.route('/')
def index():
	return template(SERVERPATH + '/templates/root')

## Hier wird die Standardroute erneut implementiert, allerdings wird diese Route nur aufgerufen sollte die Seite via POST aufgerufen werden.
# Da in der /templates/root ein Formular eingebettet ist welches seine Daten via POST überträgt muss hier definiert werden wie die Frmulardaten verarbeitet werden.
# Eine Variable upload wird deklariert in welcher die ankommenden Daten aus dem Formular angenommen werden.
# Diese Daten werden in eine name und eine ext (Extension Variable getrennt).
# Ext wird dann auf den Dateityp kontrolliert und der Benutzer bekommt eine Fehlermeldung, sollte er eine unerlaubte Datei hochgeladen haben.
# Bei einem korrekten Upload wird die Datei im Ordner midi/ gespeichert und der Benutzer wird auf die Seite zum Abspielen weitergeleitet.

@app.route('/', method='post')
def up():
	upload = request.files.get("upload")

	if upload == None:
		return template(SERVERPATH + '/templates/error/no_file')

	name, ext = os.path.splitext(upload.filename)
	
	if ext not in (".midi",".mid"):
		return template(SERVERPATH + '/templates/error/wrong_file')

	save_path=SERVERPATH + '/midi/' + upload.filename
	upload.save(save_path, overwrite=True)
	return redirect('/play/' + upload.filename)


## Diese Route listet alle bereits hochgeladenen Dateien auf. Es wird zwischen vom Benutzer hochgeladenen Dateien und kuratierten (auf jeden Fall funktionierenden) Dateien unterschieden.
# Die Dateien liegen in unterschiedlichen Ordnern.
# f und g sind zwei Listen die jeweils die entsprechenden Dateien beinhalten.
# f beinhaltet die vom Benutzer hochgeladenen Dateien
# g beinhaltet die kuratierten Dateien
# Die beiden for Schleifen iterieren jeweils über die angegebenen Verzeichnisse und schreiben dabei alle Dateien die gefunden werden in die Listen.
# Das Template gibt dann beide Listen aufbereitet aus.
# data und data2 sind die entsprechenden Variablen welche von dem Template für die Darstellung genutzt werden.

@app.route('/uploads')
def list():
	f = []
	g = []
	for (dirpath, dirnames, filenames) in os.walk(SERVERPATH + '/midi/'):
		f.extend(filenames)
		break
	for (dirpath, dirnames, filenames) in os.walk(SERVERPATH + '/midi/curated/'):
		g.extend(filenames)
		break
	return template(SERVERPATH + '/templates/uploads', data=f, data2=g)


## Diese Route wird zusammen mit dem Namen der Midi Datei aufgerufen die abgespielt werden soll.
# Bottle schaut dann nach, in welchem Verzeichnis sich die Datei befindet und schreibt dann den entsprechenden
# Befehl inklusive der Pfadangabe für die Midi Datei in die Input Pipe des Daemons.

@app.route('/play/<midi>')
def play(midi):

	os.write(to_daemon, 'status\n')

	if print_response() == 'STATUS_PLAYING\n':
		return template(SERVERPATH + '/templates/error/playing')
	else:

		if os.path.isfile(SERVERPATH + '/midi/' + midi):
			os.write(to_daemon, 'play\n' + SERVERPATH + '/midi/' + midi + '\n')
		else:
			os.write(to_daemon, 'play\n' + SERVERPATH + '/midi/curated/' + midi + '\n')

## Auf Antwort des Daemons warten ob Abspielen geklappt hat.

		if print_response() == 'PLAY_OK\n':
			print "Alles klar"
			return template(SERVERPATH + '/templates/play', file=midi)
		else:
			print "Fehler"
			return template(SERVERPATH + '/templates/playerror', file=midi)
	
	
	



@app.route('/stop')
def stop():
	os.write(to_daemon, 'fstop\n')

	return redirect('/')




if __name__ == "__main__":


	def print_response():
		with open(PIPE_FROM, 'r') as f:
			response = f.readline()
			print "daemon returns " + response
		return response


	## Test ob FIFOs für die Kommunikation mit dem Daemon vorhanden sind

	try:
		to_daemon = os.open(PIPE_TO, os.O_WRONLY | os.O_NONBLOCK)
	except OSError:
		print "Pipes sind nicht vorhanden. Läuft der Daemon? -> Startvorgang wird abgebrochen."
		sys.exit(1)


	## Test ob entsprechende Ordnerstruktur existiert
	# Falls der Ordner in welchem die Midi Dateien gespeichert werden sollen nicht existiert wird er vor dem Start des eigentlichen
	# Servers angelegt.

	if not os.path.exists(SERVERPATH + '/midi/'):
		os.makedirs(SERVERPATH + '/midi/curated')


	## Test ob Ordner mit Templates / Statischem Inhalt existieren
	# Falls der Template Ordner oder der Ordner mit dem Statischen Inhalt (Bildern) fehlt, wird eine entsprechende Fehlermeldung
	# zurückgegeben
	if not	os.path.exists(SERVERPATH + '/templates/'):
		print "Template Ordner konnte nicht gefunden werden -> Breche ab."
		sys.exit(1)

	if not os.path.exists(SERVERPATH + '/static/'):
		print "Static Ordner konnte nicht gefunden werden -> Breche ab."
		sys.exit(1)

	## Start des Servers
	# @param app Bottle app die vom Server ausgeliefert wird.
	# @param host Netzwerkinterface an welches der Server gebunden wird. In diesem Fall an alle vorhandenen Interfaces.
	# @param port Port auf welchem der Server auf eingehende Verbindungen wartet.
	# @param reloader Sofern dieses Parameter auf True gesetzt wird, startet Python den Server automatisch nach Änderungen am Quellcode neu.
	# @param debug True setzt das Debug Flag, welches eine ausführlichere Fehleranalyse erlaubt.

	run (app, host='0.0.0.0', port=8080, reloader=True, debug=True)