<!DOCTYPE html>
	<head>
		<meta charset=utf-8 />
		<title>Keine Datei übergeben</title>
		<style>
			body {margin-left:5%}
		</style>
	</head>
	<body>
		<h2>Keine Datei angegeben</h2>
		<p><img src="/static/images/facepalm.png" width=300px></p>
		<p>Wir können nichts abspielen wenn du nichts hochlädtst.</p>
		<p><a href="/">Nochmal versuchen?</a> | <a href="/uploads"> Oder zu den bereits hochgeladenen Dateien gehen?</a></p>
	</body>
</html>