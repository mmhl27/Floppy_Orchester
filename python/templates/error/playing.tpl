<!DOCTYPE html>
	<head>
		<meta charset=utf-8 />
		<title>Datei wird abgespielt</title>
		<style>
			body {margin-left:5%}
		</style>
	</head>
	<body>
		<h2>Es wird gerade eine Datei abgespielt</h2>
		<p><img src="/static/images/facepalm.png" width=300px></p>
		<p><a href="/">Datei hochladen</a> | 
		<a href="/uploads">Hochgeladene Dateien ansehen</a></p>
		<p>Im Moment wird noch eine Datei abgespielt. Warte einfach bis die Wiedergabe fertig ist oder</p>
		<p><a href="/stop">stoppe die Wiedergabe.</a></p>
	</body>
</html>