<!DOCTYPE html>
	<head>
		<meta charset=utf-8 />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
		<title>Playing {{file}}</title>
		<style>
			body {margin-left:5%}
		</style>
	</head>
	<body>
		<p><img src="/static/images/floppy_orchester.png" height="200px"></p>
		<p><a href="/">Datei hochladen</a> | 
		<a href="/uploads">Hochgeladene Dateien ansehen</a></p>
		<h2>{{file}} wird bearbeitet und abgespielt</h2>
		<p><img src="/static/images/loading.gif"></p>
		<p>Wir arbeiten hart daran, deine Datei zu bearbeiten und dann abzuspielen. Bitte habe einen Moment Geduld. Das Konzert beginnt gleich.</p>
		<p><a href="/stop">Die Wiedergabe stoppen</a></p>
	</body>
</html>

