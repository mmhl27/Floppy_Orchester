<!DOCTYPE html>
	<head>
		<meta charset=utf-8 />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
		<title>Uploads</title>
		<style>
			body {margin-left:5%}
		</style>
	</head>
	<body>
		<p><img src="/static/images/floppy_orchester.png" height="200px"></p>
		<p><a href="/">Midi hochladen</a></p>
		<h3>Hochgeladene Midis</h3>
		<p><ul style="list-style-type: none">
			% for item in data:
			<a href="/play/{{item}}"><li style="color:red">{{item}}</li></a>
			% end
		</ul></p>
		<p><h3>Von uns getestete Midis.</h3></p>
		<p><ul style="list-style-type: none">
			% for item in data2:
			<a href="/play/{{item}}"><li style="color:green">{{item}}</li></a>
			% end
		</ul></p>	
	</body>
</html>
